from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def weather_data(city, state):
    params = {
        "q": f"{city}, {state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None


# import to views for list locations.
def get_photo(city, state):
    # dictionary for security information
    headers = {"Authorization": PEXELS_API_KEY}
    # dictionary of parameters per Pexels documentation
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    # url variable. link from pexels documentation with city and state plugged in
    url = "http://api.pexels.com/v1/search?query={city}+{state}"
    response = requests.get(url, params, headers=headers)
    print(response)
    return response.json()["photos"][0]["url"]
